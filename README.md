zipkin-dependencies
===

A wrapper for [openzipkin/zipkin-dependencies](https://github.com/openzipkin/zipkin-dependencies)
that allows us to run the container as an arbitrary user, by overriding
some metadata in the Docker image.


